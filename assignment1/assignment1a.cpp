// C program to demonstrate use of fork() and file()
//parallel programming using fork and file
//name pankaj kumar 
// roll 111601014
// parallel programming assignment
#include <fstream>
#include <iostream> 
#include <time.h>  
#include<stdio.h> 
#include<stdlib.h> 
#include<unistd.h> 
#include<sys/types.h> 
#include<string.h> 
#include<sys/wait.h>
using namespace std;

int main() 
{ 
    pid_t child_a, child_b , child_c;
    char data1[100],data2[100];
    int th=0,ts=0,loop=0;

child_a = fork();

if (child_a == 0) {
    /* Child A code */
    char time1[100];
        ifstream time_counter;
        time_counter.open("time_counter.txt");
        time_counter>>time1;
while(atoi(time1)<10000)
{
ofstream outfile_hare;
ifstream infile_turtle;
   outfile_hare.open("hare_counter.txt");
   infile_turtle.open("tortoise_counter.txt");
   

   infile_turtle >> data1; 
    if(th>atoi(data1)+1000)
    {
int sleep = rand() % 100 + 1;
usleep(sleep);
    } /*hare*/

   //    while(1) the file.
   int step = rand() % 10 + 1;
   th=th+step;
   outfile_hare << th<< endl;
   outfile_hare.close();
   infile_turtle.close();
    }
} else 
{
    child_b = fork();
    

    if (child_b == 0) {
         char time1[100];
        ifstream time_counter;
        time_counter.open("time_counter.txt");
        time_counter>>time1;
while(atoi(time1)<10000)
{
         
    ofstream outfile_tortoise;
     outfile_tortoise.open("tortoise_counter.txt");
   // write inputted data into the file.
   ts=ts+1;
   outfile_tortoise << ts<< endl;
   outfile_tortoise.close();
    }
    } else {
        child_c= fork();

    if (child_c == 0) {
        /* Child C code */
        /*reporter*/
        int m=0;
        char time1[100];
        ifstream time_counter;
        time_counter.open("time_counter.txt");
        time_counter>>time1;
while(atoi(time1)<10000)
{
    m++;
    if(m==100)
    {
    ifstream infile_turtle,infile_hare; 
   infile_turtle.open("tortoise_counter.txt");
   infile_hare.open("hare_counter.txt");  
   infile_turtle >> data1; 
    infile_hare >> data2; 

   // write the data at the screen.
   cout <<"tortoise"<< data1 << endl;
   cout <<"hare"<< data2<< endl;
   
   // again read the data from the file and display it.
  
   

   // close the opened file.
   infile_hare.close();
   infile_turtle.close();
   m=0;
    }
}
    }else{
        /* Parent Code */
        /*god*/ 
        ofstream time_counter;
        int j=0;
        int t=0;
        int gk=0;
        while(1)
        { 
            j++;
             time_counter.open("time.txt");
             time_counter<<t%1000<<endl;
            if(j==10000)
            {
                gk=gk+1;
        ofstream outfile_tortoise, outfile_hare;
     outfile_tortoise.open("tortoise_counter.txt");
     outfile_hare.open("hare_counter.txt");
    

   // write inputted data into the file.
   outfile_tortoise << 0<< endl;
   outfile_tortoise << 0<< endl;
   outfile_tortoise.close();
   outfile_hare.close();
   j=0;
   if(gk==5)
   {
       break;
   }
            }
        }
        time_counter.close();

    }
    }
}
}