// assignment 1b thread version 
// compile with g++ threadversion.cpp -pthread
// name pankaj kumar
// roll 111601014 //#slow and steady wins the race;
#include <iostream> 
#include <thread> 
#include <fstream>
#include <iostream> 
#include <time.h>  
#include<stdio.h> 
#include<stdlib.h> 
#include<unistd.h> 
#include<sys/types.h> 
#include<string.h> 
#include<sys/wait.h>
using namespace std; 
  
// A dummy function 
int ts=0,th=0,time_t1=0;
void tortoise() 
{ 
    while(ts<100000)
    {
ts=ts+1;
    }
    
} 

void hare() 
{ 
    while(th<100000)
    {
     if(th>ts+1000)
    {
int sleep = rand() % 100 + 1;
usleep(sleep);
    } /*hare*/

   int step = rand() % 10 + 1;
   th=th+step;
    }
} 
void reporter() 
{ int k=0;
    while(k<100)
    {
        k++;
    cout<<"\ntortoise"<<ts;
    cout<<"\nhare "<<th;
    }
    
} 


int main() 
{ 
    cout<<"starting position of turtle";
    cin>>ts;
    cout<<"starting position of hare";
    cin>>th;
     thread th2(hare); 
    thread th1(tortoise); 
   
    thread th3(reporter);
    while(time_t1<10000)
    {
        time_t1++;
    }
   th1.join(); 
  
    // Wait for thread t2 to finish 
    th2.join(); 
  
    // Wait for thread t3 to finish 
    th3.join();
    return 0; 
} 