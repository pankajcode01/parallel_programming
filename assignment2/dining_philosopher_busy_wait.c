#include <pthread.h> 
#include <semaphore.h> 
#include <stdio.h> 
  
#define N 5 
#define THINKING 2 
#define HUNGRY 1 
#define EATING 0 
#define LAST_BITE 3
#define LEFT (phnum + 4) % N 
#define RIGHT (phnum + 1) % N 
  
int state[N]; 
int phil[N] = { 0, 1, 2, 3, 4 };
int bites[N] = { 0, 0, 0, 0, 0};

int forks[N] = {0,0,0,0,0};
  
void test(int phnum) 
{ 
    if (state[phnum] == HUNGRY 
        && state[LEFT] != EATING 
        && state[RIGHT] != EATING) { 
        // state that eating 
        state[phnum] = EATING; 
        bites[phnum] = bites[phnum]+1;
        printf("Philosopher %d takes fork %d and %d\n", 
                      phnum + 1, LEFT + 1, phnum + 1); 
        printf("Philosopher %d is Eating\n", phnum + 1); 
        // sem_post(&S[phnum]) has no effect 
        // during takefork 
        // used to wake up hungry philosophers 
        // during putfork 
        
    } 
} 
  
// take up chopsticks 
void take_fork(int phnum) 
{   
     if(forks[LEFT]!=0 && forks[RIGHT]!=0)
{
  while(forks[LEFT]!=0 && forks[RIGHT]!=0)  
  {

  }
}

    else if(forks[LEFT]==0 && forks[RIGHT]==0)
{
    // state that hungry 
    state[phnum] = HUNGRY; 
    printf("Philosopher %d is Hungry\n", phnum + 1); 

    // eat if neighbours are not eating 
      forks[LEFT]==1 ;
    forks[RIGHT]==1;
       printf("Philosopher %d takes fork %d and %d\n", 
                      phnum + 1, LEFT + 1, phnum + 1); 
        printf("Philosopher %d is Eating\n", phnum + 1); 
}
    // if unable to eat wait to be signalled 
    
} 
  
// put down chopsticks 
void put_fork(int phnum) 
{ 
    if(forks[LEFT]!=1 && forks[RIGHT]!=1)
{
  while(forks[LEFT]!=1 && forks[RIGHT]!=1)  
  {

  }
}

    else if(forks[LEFT]==1 && forks[RIGHT]==1)
{

      // state that thinking 
    state[phnum] = THINKING; 
    forks[LEFT]==0;
    forks[RIGHT]==0;
    printf("Philosopher %d putting fork %d and %d down\n", phnum + 1, LEFT + 1, phnum + 1); 
    printf("Philosopher %d is thinking\n", phnum + 1);  
}
     
} 
  
void* philospher(void* num) 
{ 
      while (1) { 
           int* i = num; 
          if(bites[*i]!=LAST_BITE)
          {
       
        sleep(1); 
        take_fork(*i); 
        sleep(0); 
        put_fork(*i); 
          }
          else
          {
            printf("Philosopher %d finish eating \n", *i+ 1); 
              break;
          }
    } 
} 
  
int main() 
{ 
  
    int i; 
    pthread_t thread_id[N];
    // initialize the semaphores 
    for (i = 0; i < N; i++) {  
        // create philosopher processes 
        pthread_create(&thread_id[i], NULL, 
                       philospher, &phil[i]); 
        printf("Philosopher %d is thinking\n", i + 1); 
    }
    for (i = 0; i < N; i++) 
        pthread_join(thread_id[i], NULL); 
} 
