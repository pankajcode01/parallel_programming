// assignment 1b thread version 
// compile with g++ threadversion.cpp -pthread
// name pankaj kumar
// roll 111601014 //#slow and steady wins the race;
#include <iostream> 
#include <thread> 
#include <fstream>
#include <iostream> 
#include <time.h>  
#include<stdio.h> 
#include<stdlib.h> 
#include<unistd.h> 
#include<sys/types.h> 
#include<string.h> 
#include<sys/wait.h>
using namespace std; 
  
// A dummy function 
float sum=0;
int n=10000;
void sum1() 
{ 
   for(float i=0 ;i<n;i=i+4)
   {
       sum=sum+1/(2*i+1);
        printf("\n %f , t1",sum*4);
   }
    
} 
void sum2() 
{ 
   for(float i=2 ;i<n;i=i+4)
   {
       sum=sum+1/(2*i+1);
        printf("\n %f , t2",sum*4);
   }
    
} 
void sum3() 
{ 
   for(float i=1 ;i<n;i=i+4)
   {
       sum=sum-1/(2*i+1);
        printf("\n %f , t3",sum*4);
   }
    
} 
void sum4() 
{ 
   for(float i=3 ;i<n;i=i+4)
   {
       sum=sum-1/(2*i+1);
        printf("\n %f , t4",sum*4);
   }
    
} 

int main() 
{ 
    
     thread th1(sum1); 
     thread th2(sum2); 
     thread th3(sum3);
     thread th4(sum4);
      th1.join();
    th2.join();
    th3.join();
    th4.join(); 
    printf("\n %f",sum*4);
  
    
    return 0; 
} 